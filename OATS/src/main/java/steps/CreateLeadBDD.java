/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadBDD {
	public static ChromeDriver driver;

	@Given("launch the browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
	}

	@And("Maximize the window")
	public void maximizeBrowser() {
		driver.manage().window().maximize();
	}

	@And ("Set Timeouts")
	public void setTimeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@And ("enter URL")
	public void enterURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And ("Enter UserName as (.*)")
	public void enterUserName(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}
	@And ("Enter Password as (.*)")
	public void enterPassword(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@And ("Click on Login Button")
	public void clickLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@And ("Click on CRMSFA Link")
	public void clickCRMLink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@And ("Click on Create Lead Link")
	public void clickCreateLeadlnk() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@And ("Enter Company Name as (.*)")
	public void enterCmpName(String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}

	@And ("Enter First Name as (.*)")
	public void enterFName(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}

	@And ("Enter Last Name as (.*)")
	public void enterLName(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@When ("Create Lead Button is Clicked")
	public void clickCreateLeadBut() {
		driver.findElementByName("submitButton").click();
	}

	@Then ("Lead Should be Created Successfully")
	public void verifyLead() {
		System.out.println("Lead Created Successfully");
	}

}
*/