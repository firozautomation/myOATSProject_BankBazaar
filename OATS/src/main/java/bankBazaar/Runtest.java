package bankBazaar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
public class Runtest extends ProjectMethods{
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_BankBazzar test";
		testCaseDescription ="Test Bank Bazzar";
		category = "Smoke";
		author= "Syed";
		dataSheetName="bankbazaar";
	}
	//String age="28",month="Jan 1990",day="26",salary="50000";
	@Test(dataProvider="fetchData")
	public void testcase(String age,String month,String day,String salary,String fname) {
		new Homepage().launchbrowser()
		.mouseoverinvestment()
		.clickmutual()
		.clicksearchmutualfund()
		.AgeSelector(age)
		.clickmonth(month)
		.clickday(day)
		.clickcontinue()
		.typesalary(salary)
		.clickcontinue1()
		.selectbank()
		.typefirstname(fname)
		.viewmutualfund()
		.viewfunds();
	}
}
