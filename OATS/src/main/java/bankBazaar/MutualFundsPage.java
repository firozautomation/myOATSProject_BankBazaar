package bankBazaar;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.SeMethods;

public class MutualFundsPage extends SeMethods{
	public MutualFundsPage clicksearchmutualfund() {
		WebElement searchmutual = locateElement("linktext", "Search for Mutual Funds");
		click(searchmutual);
		return this;
	}

	public MutualFundsPage AgeSelector(String age) {
		WebElement ageslider = locateElement("class", "rangeslider__handle-label");
		Actions action = new Actions(driver);
		//while (ageslider.getText()!=age) {
		int ageint = Integer.parseInt(age);
		//ageslider.getLocation();
		action.dragAndDropBy(ageslider, (ageint-18)*8, 0).perform();		
		//}
		return this;
	}
	public MutualFundsPage clickmonth(String month) {
		WebElement mth = locateElement("xpath", "//a[contains(text(),'"+month+"')]");
		click(mth);
		return this;
	}
	public MutualFundsPage clickday(String day) {
		WebElement dayloc = locateElement("xpath", "//div[text()='"+day+"']");
		click(dayloc);
		return this;
	}
	public MutualFundsPage clickcontinue() {
		WebElement cnt = locateElement("linktext", "Continue");
		click(cnt);
		return this;
	}
	public MutualFundsPage typesalary(String salary) {
		WebElement incometext = locateElement("name", "netAnnualIncome");
		type(incometext, salary);
		return this;
	}
	public MutualFundsPage clickcontinue1() {
		WebElement cont = locateElement("linktext", "Continue");
		click(cont);
		return this;
	}
	public MutualFundsPage selectbank() {
		WebElement bank = locateElement("xpath", "//span[text()='HDFC']");
		click(bank);
		return this;
	}
	public MutualFundsPage typefirstname(String fname) {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement fnametxt = locateElement("xpath", "//input[@name='firstName']");
		type(fnametxt, fname);
		return this;
	}
	public MutualFundsPage viewmutualfund() {
		WebElement view = locateElement("linktext", "View Mutual Funds");
		click(view);
		//dismissAlert();
		return this;
	}
	public MutualFundsPage viewfunds() {
		List<WebElement> funds = driver.findElementsByXPath("//*[@id=\"offer-table\"]/section/div[1]/div/div[1]/span/span[2]/nav/ul/li/span");
		for (WebElement fund : funds) {
			String schemename = fund.getText();
			System.out.println("Schemename: "+schemename);
		}
		return this;
	}
}
