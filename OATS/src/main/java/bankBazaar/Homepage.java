package bankBazaar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.SeMethods;

public class Homepage extends SeMethods{

	public Homepage launchbrowser() {
		startApp("chrome", "https://www.bankbazaar.com/");
		return this;
	}

	public Homepage mouseoverinvestment() {
		WebElement investment = locateElement("linktext", "INVESTMENTS");
		Actions action = new Actions(driver);
		action.moveToElement(investment).perform();;
		return this;
	}

	public MutualFundsPage clickmutual() {
		WebElement mutualfund = locateElement("linktext", "Mutual Funds");
		click(mutualfund);
		return new MutualFundsPage();
	}
}
