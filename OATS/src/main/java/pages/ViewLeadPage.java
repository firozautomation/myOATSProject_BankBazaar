package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	@Then ("Lead Should be Created Successfully")
	public void verifylead() {
		System.out.println("Lead Created");
	}
	public EditLeadPage clickEdit() {
		WebElement eleEditButton= locateElement("linktext", "Edit");
		click(eleEditButton);
		return new EditLeadPage() ; 
	}
	public void clickDelete() {
		WebElement eleDeleteButton= locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleDeleteButton);
		//return void ; 
	}
}
