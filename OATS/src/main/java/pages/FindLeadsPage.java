package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage clickPhone() {
		WebElement elephonetab = locateElement("xpath", "//span[text()='Phone']");
		click(elephonetab);
		return this;
	}

	public FindLeadsPage clickEmail() {
		WebElement eleemailtab = locateElement("xpath", "//span[text()='Email']");
		click(eleemailtab);
		return this;
	}
	public FindLeadsPage typephonenumber(String data) {
		WebElement elephonenum = locateElement("name", "phoneNumber");
		type(elephonenum, data);
		return this;
	}
	public FindLeadsPage typeEmail(String data) {
		WebElement eleEmail = locateElement("name", "emailAddress");
		type(eleEmail, data);
		return this;
	}
	public FindLeadsPage clickfindleades() {
		WebElement elefindleadbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindleadbutton);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public ViewLeadPage clickresultid() {
		WebElement eleresult1 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleresult1);
		return new ViewLeadPage();
	}
}
