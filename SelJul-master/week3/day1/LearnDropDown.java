package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnDropDown {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		//maximize
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username")
		.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("600100",Keys.TAB);
		Thread.sleep(3000);
		// Click Login
		driver.findElementByClassName("decorativeSubmit")
		.click();
		driver.findElementByLinkText("CRM/SFA")
		.click();
		driver.findElementByLinkText("Create Lead")
		 .click();
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(src);
		//dd.selectByVisibleText("Employee");
		//dd.selectByValue("LEAD_DIRECTMAIL");
		//dd.selectByIndex(6);
		List<WebElement> allOptions = dd.getOptions();
		System.out.println(allOptions.size());
		for (WebElement eachOption : allOptions) {
			if(eachOption.getText().startsWith("E")) {
				System.out.println(eachOption.getText());
			}
		}
		







	}
}
