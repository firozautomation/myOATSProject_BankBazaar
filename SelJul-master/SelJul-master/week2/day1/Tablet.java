package week2.day2;

public interface Tablet {
	
	public void browse();
	
	public void installApp();

}
