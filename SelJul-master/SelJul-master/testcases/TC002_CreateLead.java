package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeClass
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Koushik";
	}

	@Test
	public void createLead() {
		//login();
		WebElement eleCrateLead = locateElement("link","Create Lead");
		click(eleCrateLead);
		WebElement eleCN = locateElement("id","createLeadForm_companyName");
		type(eleCN,"TL");
		WebElement eleFN = locateElement("id","createLeadForm_firstName");
		type(eleFN,"Sarath");
		WebElement eleLN = locateElement("id","createLeadForm_lastName");
		type(eleLN,"M");
		WebElement eleCreateLeadButton = locateElement("class","smallSubmit");
		click(eleCreateLeadButton);

	}

}








