package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC003_ED extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC003";
		testCaseDescription ="Edit lead";
		category = "Sanity";
		author= "Sethu";
	}
	@Test
	public void edit() {
		//login();
		WebElement eleCrateLead = locateElement("link","Create Lead");
		click(eleCrateLead);
		WebElement eleCN = locateElement("id","createLeadForm_companyName");
		type(eleCN,"TL");
		WebElement eleFN = locateElement("id","createLeadForm_firstName");
		type(eleFN,"Sarath");
		WebElement eleLN = locateElement("id","createLeadForm_lastName");
		type(eleLN,"M");
		WebElement eleCreateLeadButton = locateElement("class","smallSubmit");
		click(eleCreateLeadButton);
		
	}

}








