package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://legacy.crystalcruises.com/");
		String windowHandle = driver.getWindowHandle();
		System.out.println(windowHandle);
		Set<String> allWindows = driver.getWindowHandles();
		System.out.println(allWindows.size());
		driver.findElementByLinkText("GUEST CHECK-IN").click();
		Set<String> allWindows1 = driver.getWindowHandles();
		System.out.println(allWindows1.size());
		String title = driver.getTitle();
		System.out.println(title);
		List<String> listOfWindows = new ArrayList<String>();
		listOfWindows.addAll(allWindows1);
		String secondWindow = listOfWindows.get(1);
		driver.switchTo().window(secondWindow);
		String newTitle = driver.getTitle();
		System.out.println(newTitle);
		//driver.close();
		WebElement activeElement = driver.switchTo().activeElement();
		System.out.println(activeElement.getAttribute("id"));
		driver.quit();
		
		
	}
	
	
	 
	
	
	
	
	

}
