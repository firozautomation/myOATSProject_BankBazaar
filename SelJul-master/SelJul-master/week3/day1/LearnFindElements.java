package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		// Create obj
		ChromeDriver driver = new ChromeDriver();
		// Load the URL
		driver.get("http://www.crystalcruises.com/cruises/calendar?year=2018");
		// Find elements
		List<WebElement> allReq = 
				driver.findElementsByLinkText("Request A Quote".toUpperCase());
		// print size
		System.out.println(allReq.size());
		// click on the 3rd link
		allReq.get(2).click();
		// print all links
		for (WebElement eachReq : allReq) {
			System.out.println(eachReq.getText());
		}
		
		
		
		
		
		
		
		
		
	}

}
