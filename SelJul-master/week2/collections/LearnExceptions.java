package week2.collections;

import java.util.InputMismatchException;
import java.util.Scanner;

public class LearnExceptions {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		try {
			//Thread.sleep(123);
			int a = scan.nextInt();
			int b = scan.nextInt();
			System.out.println("Answer is "+a/b);
		}
		/*catch (ArithmeticException e) {
			System.out.println("Should not enter Input 0"+e.getMessage());
		}catch (Exception e) {
			System.out.println("Should not enter Input char");
		}*/ finally {
			System.out.println("Always Run");
		}

	}

}











